package plugins;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import no.cax.Vore.Vore;
import no.cax.Vore.VoreDB;

public class ForgeListener implements PluginMessageListener {

	public final static String channele = "vorecraft:data";

	static enum PacketDiscriminators {
		REGISTER, STOMACH_UPDATE, STOP, EATEN, SHRINK
	}

	@Override
	public void onPluginMessageReceived(String channel, Player player, byte[] payload) {
		if (!channel.equalsIgnoreCase(channele))
			return;
		if (payload.length == 0)
			return;
		PacketDiscriminators disc = PacketDiscriminators.values()[payload[0]];
		VoreDB db = Vore.PlayerDB.get(player.getUniqueId());
		
		if (db == null && disc != PacketDiscriminators.REGISTER && disc != PacketDiscriminators.SHRINK) {
			// how?
			return;
		}
		if(db.isForgeMode() && disc == PacketDiscriminators.SHRINK) {
			if(payload.length < 3) {
				return;
			}
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
			byteArrayOutputStream.write(PacketDiscriminators.SHRINK.ordinal());
			byteArrayOutputStream.write(payload[1]);
			byteArrayOutputStream.write(payload[2]);
			byteArrayOutputStream.write(payload[3]);
			writeString(byteArrayOutputStream, db.getThisPlayer().toString()); //UUID in string format
			final byte[] p = byteArrayOutputStream.toByteArray();
			
			Bukkit.getScheduler().scheduleSyncDelayedTask(Vore.getPlugin(Vore.class), new Runnable() {

				@Override
				public void run() {
					
					for (Player playere : Bukkit.getOnlinePlayers()) {
						if (player.getWorld().getUID().equals(playere.getWorld().getUID())
								&& player.getLocation().distanceSquared(playere.getLocation()) < 1000) {
							VoreDB dg = Vore.PlayerDB.get(playere.getUniqueId());
							if (dg != null) {
								onShrinkSizeUpdate(playere,dg,p);
							}
						}
					}
				}
				
			},20);
			return;
		}
		if(db.isForgeMode()) {
			return;
		}
		db.setForgeMode(true);
		
		//create packet and send
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		byteArrayOutputStream.write(PacketDiscriminators.REGISTER.ordinal());
		final byte[] p = byteArrayOutputStream.toByteArray();

		Bukkit.getScheduler().scheduleSyncDelayedTask(Vore.getPlugin(Vore.class), new Runnable() {

			@Override
			public void run() {
				player.sendPluginMessage(Vore.getPlugin(Vore.class), channele, p);
				
				for (Player playere : Bukkit.getOnlinePlayers()) {
					if (player.getWorld().getUID().equals(playere.getWorld().getUID())
							&& player.getLocation().distanceSquared(playere.getLocation()) < 1000) {
						VoreDB dg = Vore.PlayerDB.get(playere.getUniqueId());
						if (dg != null) {
							if (dg.hasEaten.size() > 0) {
								onStomachNumberUpdate(dg, player);
							}
						}
					}
				}
			}
			
		},20);
		
		return;
	}
	
	protected void onShrinkSizeUpdate(Player player, VoreDB dg, byte[] payload) {
		if(dg.isForgeMode()) {
			player.sendPluginMessage(Vore.getPlugin(Vore.class), channele, payload);
		}
	}

	//called to update stomachs from internal stuff
	public static void updateStomach(VoreDB voreDB) {
		for (VoreDB db : Vore.PlayerDB.values()) {
			if (db.isForgeMode()) {
				Player target = Bukkit.getPlayer(db.getThisPlayer());
				if (target != null) {
					onStomachNumberUpdate(voreDB, target);
				}
			}
		}
	}

	/* predtype enum: 
	 * oral, anal, cock, tail, unbirth, absorb, breast, navel, soul, pouch, bladder,
	 * unused, food, custom,
	 */
	/* Mode enum: 
	 * none, prey, pred, predprey, freeprey, freepred, freepredprey
	 */
	static // sends update when stomach contents changed with amount of players inside
	void onStomachNumberUpdate(VoreDB db, Player target) {
		final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

		byteArrayOutputStream.write(PacketDiscriminators.STOMACH_UPDATE.ordinal());
		byteArrayOutputStream.write(db.hasEaten.size());
		byteArrayOutputStream.write(db.getPredType().ordinal());
		byteArrayOutputStream.write(db.mode.ordinal());
		writeString(byteArrayOutputStream, db.getThisPlayer().toString()); //UUID in string format

		final byte[] p = byteArrayOutputStream.toByteArray();
		target.sendPluginMessage(Vore.getPlugin(Vore.class), channele, p);
	}

	public static// all vo stuff ends or ejected from belly /death etc
	void onStop(VoreDB db, Player target) {
		if (db.isForgeMode()) {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

			byteArrayOutputStream.write(PacketDiscriminators.STOP.ordinal());
			final byte[] p = byteArrayOutputStream.toByteArray();
			target.sendPluginMessage(Vore.getPlugin(Vore.class), channele, p);
		}
	}

	/*
	 * oral, anal, cock, tail, unbirth, absorb, breast, navel, soul, pouch, bladder,
	 * unused, food, custom,
	 */
	public static // sends update when eaten by pred, number is predtype
	void onEaten(VoreDB db, Player target, Vore.PredTypes type) {
		if (db.isForgeMode()) {
			final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

			byteArrayOutputStream.write(PacketDiscriminators.EATEN.ordinal());
			byteArrayOutputStream.write(type.ordinal());

			final byte[] p = byteArrayOutputStream.toByteArray();
			target.sendPluginMessage(Vore.getPlugin(Vore.class), channele, p);
		}
	}

	// -------------------------UTILITY METHODS BELOW THIS POINT!-------------------------------------------------
	public static byte[] StringToPayload(PacketDiscriminators version, String input) {
		ByteArrayOutputStream output = new ByteArrayOutputStream();

		output.write((byte) version.ordinal());
		if (!writeString(output, input)) {
			output.reset();
			return output.toByteArray();
		}

		return output.toByteArray();

	}

	public static boolean writeString(ByteArrayOutputStream output, String str) {
		byte[] bytes = str.getBytes(StandardCharsets.UTF_8);
		int len = bytes.length;
		try {
			if (!writeVarInt(output, len, 2))
				return false;
			output.write(bytes);
		} catch (IOException e) {
			return false;
		}

		return true;
	}

	public static int varIntByteCount(int toCount) {
		return (toCount & 0xFFFFFF80) == 0 ? 1
				: ((toCount & 0xFFFFC000) == 0 ? 2
						: ((toCount & 0xFFE00000) == 0 ? 3 : ((toCount & 0xF0000000) == 0 ? 4 : 5)));
	}

	public static boolean writeVarInt(ByteArrayOutputStream to, int toWrite, int maxSize) {
		if (varIntByteCount(toWrite) > maxSize)
			return false;
		while ((toWrite & -128) != 0) {
			to.write(toWrite & 127 | 128);
			toWrite >>>= 7;
		}

		to.write(toWrite);
		return true;
	}

}
