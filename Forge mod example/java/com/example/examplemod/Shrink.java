package com.example.examplemod;

import net.minecraft.client.renderer.entity.model.AgeableModel;
import net.minecraft.client.renderer.entity.model.EntityModel;
import net.minecraft.entity.EntitySize;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.Pose;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraftforge.client.event.RenderLivingEvent;
import net.minecraftforge.client.event.RenderPlayerEvent;
import net.minecraftforge.event.entity.EntityEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

public class Shrink {

	@SubscribeEvent
	public static void changeSize(EntityEvent.Size event) {
		if (event.getEntity() instanceof LivingEntity) {
			LivingEntity livingEntity = (LivingEntity) event.getEntity();
			double x = event.getEntity().getPosX();
			double y = event.getEntity().getPosY();
			double z = event.getEntity().getPosZ();
			boolean isShrunk = true;
			if (isShrunk && event.getPose() == Pose.STANDING) {
				event.setNewSize(new EntitySize(0.1F, 0.2F, true));
				event.setNewEyeHeight(0.16F);
				event.getEntity().setPosition(x, y, z);
			} else if (isShrunk && event.getPose() == Pose.CROUCHING) {
				event.setNewSize(new EntitySize(0.1F, 0.14F, true));
				event.setNewEyeHeight(0.11F);
				event.getEntity().setPosition(x, y, z);
			} else if (!isShrunk && event.getPose() == Pose.STANDING && livingEntity instanceof PlayerEntity) {
				event.setNewSize(new EntitySize(0.6F, 1.8F, false));
				event.setNewEyeHeight(1.62F);
				event.getEntity().setPosition(x, y, z);
			}
		}
	}

	@SubscribeEvent
	public void onRenderPlayerPre(RenderPlayerEvent.Pre event) {
		boolean shrunk = true;
		if (shrunk) {
			event.getMatrixStack().push();
			event.getMatrixStack().scale(0.1F, 0.1F, 0.1F);
			(event.getRenderer()).shadowSize = 0.08F;
			if (event.getEntity().isCrouching())
				event.getMatrixStack().translate(0.0D, 1.0D, 0.0D);
		} else if (!shrunk) {
			(event.getRenderer()).shadowSize = 0.05F;
		}
	}

	@SubscribeEvent
	public void onRenderPlayerPost(RenderPlayerEvent.Post event) {
		event.getMatrixStack().pop();
	}

	@SubscribeEvent
	public void onLivingRenderPre(@SuppressWarnings("rawtypes") RenderLivingEvent.Pre event) {
		try {
			LivingEntity livingEntity = event.getEntity();
			if (livingEntity != null && livingEntity instanceof net.minecraft.entity.MobEntity)
				event.getMatrixStack().push();
			event.getMatrixStack().scale(0.1F, 0.1F, 0.1F);
			(event.getRenderer()).shadowSize = 0.08F;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SubscribeEvent
	public void onLivingRenderPost(@SuppressWarnings("rawtypes") RenderLivingEvent.Post event) {
		try {
			LivingEntity livingEntity = event.getEntity();
			if (livingEntity != null && livingEntity instanceof net.minecraft.entity.MobEntity) {
				event.getMatrixStack().scale(0.5f, 0.5f, 0.5f);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
