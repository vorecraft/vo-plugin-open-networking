package com.example.examplemod;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.example.examplemod.ForgeListener.PacketDiscriminators;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.event.ClientPlayerNetworkEvent.LoggedInEvent;
import net.minecraftforge.client.event.ClientPlayerNetworkEvent.LoggedOutEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.fml.network.NetworkRegistry;
import net.minecraftforge.fml.network.simple.SimpleChannel;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("examplemod")
public class ExampleMod
{
    static final Logger LOGGER = LogManager.getLogger();
    
    private static final SimpleChannel HANDLER = NetworkRegistry.ChannelBuilder
            .named(ForgeListener.location)
            .clientAcceptedVersions(e -> true)
            .serverAcceptedVersions(e -> true)
            .networkProtocolVersion(() -> "1")
            .simpleChannel();
    
    public void preInit(FMLClientSetupEvent  event)
    {
    	HANDLER.registerMessage(PacketDiscriminators.EATEN.ordinal(), ForgeListener.class, ForgeListener::encode, ForgeListener::onEatenbyPred, ForgeListener::handle);
    	HANDLER.registerMessage(PacketDiscriminators.STOMACH_UPDATE.ordinal(), ForgeListener.class, ForgeListener::encode, ForgeListener::onStomachNumberUpdate, ForgeListener::handle);
    	HANDLER.registerMessage(PacketDiscriminators.STOP.ordinal(), ForgeListener.class, ForgeListener::encode, ForgeListener::onStop, ForgeListener::handle);
    	HANDLER.registerMessage(PacketDiscriminators.REGISTER.ordinal(), ForgeListener.class, ForgeListener::encode, ForgeListener::onRegisterWithServer, ForgeListener::handle);
    	HANDLER.registerMessage(PacketDiscriminators.SHRINK.ordinal(), ForgeListener.class, ForgeListener::encode, ForgeListener::onShrinkUpdate, ForgeListener::handle);
    	LOGGER.info("Channel registered as CLIENT Side with all enums");
    }
    
    @SubscribeEvent
    public void onPlayerLeftServer(LoggedOutEvent event) {
    	  if(event.isCanceled()) return;
    	  
  	      ForgeListener.onRegisterWithServer(false);
    	  LOGGER.info("Player left!");
    }
    
    @SubscribeEvent
    public void onPlayerJoinedServer(LoggedInEvent event) {
    	  if(event.isCanceled()) return;
    	  
    	  LOGGER.info("Player joined!");
	      sendVersionInfo();
    }
    
    public static void sendVersionInfo() {
		Minecraft.getInstance().getConnection().sendPacket(ForgeListener.getVoreCraftClientRegisterPacket());
	}
    
    public ExampleMod() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::preInit);
        
        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
        MinecraftForge.EVENT_BUS.register(Shrink.class);
        LOGGER.info("STARTUP! ");
    }
}
