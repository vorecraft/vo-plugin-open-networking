package net.fabricmc.example;

import java.util.UUID;

import io.netty.buffer.Unpooled;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.util.Identifier;

public class ForgeListener {

	public static boolean isRegistered = false;
	
	public static Identifier location = Identifier.tryParse("vorecraft:data");
    
	public static enum PacketDiscriminators {
		REGISTER, STOMACH_UPDATE, STOP, EATEN, SHRINK
	}
	
	public static enum predtype  {
		oral, anal, cock, tail, unbirth, absorb, breast, navel, soul, pouch, bladder,
		 unused, food, custom	
	};
	
	public static enum mode {
		none, prey, pred, predprey, freeprey, freepred, freepredprey
	}
	

	public static PacketByteBuf getVoreCraftClientRegisterPacket()
	{
		PacketByteBuf pb = new PacketByteBuf(Unpooled.buffer());
		pb.writeByte(PacketDiscriminators.REGISTER.ordinal());
        return  pb;
	}
	
	static ForgeListener onShrinkUpdate(PacketByteBuf buf) {
		byte x = buf.readByte();
		byte y = buf.readByte();
		byte z = buf.readByte();
		UUID thisplayer = UUID.fromString(buf.readString());
		ExampleMod.LOGGER.info("Recieved resize event from "+thisplayer+" in which the player was sized to x: "+x+" y: "+y+" z: "+z);
		return null;
	}
	
	static ForgeListener onRegisterWithServer(PacketByteBuf buf) {
		onRegisterWithServer(true);
		return null;
	}
	
	static void onRegisterWithServer(boolean state) {
		isRegistered = state;
		ExampleMod.LOGGER.info("Connection state changed to "+state);
	}

	static ForgeListener onEatenbyPred(PacketByteBuf buf) {
		predtype[] predtypevalues = predtype.values();
		predtype predtype = predtypevalues[buf.readByte()];
		
		ExampleMod.LOGGER.info("Was eaten by pred with the type : "+predtype.name());
		return null;
	}

	static ForgeListener onStomachNumberUpdate(PacketByteBuf br) {
		predtype[] predtypevalues = predtype.values();
		mode[] modevalues = mode.values();
		
		byte bellySize = br.readByte();
		predtype predtype = predtypevalues[br.readByte()];
		mode mode = modevalues[br.readByte()];
		UUID thisplayer = UUID.fromString(br.readString());
		
		ExampleMod.LOGGER.info("A belly changed size : "+bellySize+" and they were in "+mode.name()+" with type "+predtype.name()+" their uuid was "+thisplayer.toString());
		return null;
		
	}

    static ForgeListener onStop(PacketByteBuf br) {
    	ExampleMod.LOGGER.info("Recieved onstop");
		return null;
	}

}
