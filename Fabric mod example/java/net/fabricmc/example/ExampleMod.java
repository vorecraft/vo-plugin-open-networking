package net.fabricmc.example;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.example.ForgeListener.PacketDiscriminators;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientLoginConnectionEvents.Disconnect;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents.Join;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientLoginNetworkHandler;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.util.Identifier;

public class ExampleMod implements Disconnect, Join, ModInitializer {

	static final Logger LOGGER = LogManager.getLogger();

	@Override
	public void onLoginDisconnect(ClientLoginNetworkHandler handler, MinecraftClient client) {
		ForgeListener.onRegisterWithServer(false);
	}

	@Override
	public void onPlayReady(ClientPlayNetworkHandler handler, PacketSender sender, MinecraftClient client) {
		ClientPlayNetworking.send(ForgeListener.location, ForgeListener.getVoreCraftClientRegisterPacket());
	}

	@Override
	public void onInitialize() {
		System.out.println("Startup! server");
		ClientPlayNetworking.registerGlobalReceiver(ForgeListener.location, (client, handler, buf, responseSender) -> {
			try {
				PacketDiscriminators[] packetypes = PacketDiscriminators.values();
				PacketDiscriminators packetType = packetypes[buf.readByte()];

				switch (packetType) {
				case EATEN:
					ForgeListener.onEatenbyPred(buf);
					break;
				case REGISTER:
					ForgeListener.onRegisterWithServer(true);
					break;
				case SHRINK:
					ForgeListener.onShrinkUpdate(buf);
					break;
				case STOMACH_UPDATE:
					ForgeListener.onStomachNumberUpdate(buf);
					break;
				case STOP:
					ForgeListener.onStop(buf);
					break;
				default:
					break;

				}
			} catch (Exception e) {
				LOGGER.warn("Error in decoding packet", e);
			}
		});

		ClientPlayConnectionEvents.JOIN.register(this);
		ClientLoginConnectionEvents.DISCONNECT.register(this);
	}

}
